pub struct SignalTransform {
	shift_x: isize,
	shift_y: f32,
	scale_x: f64,
	scale_y: f32,
	flip_x: bool,
	flip_y: bool
}

impl Default for SignalTransform {
	fn default() -> SignalTransform {
		SignalTransform {
			shift_x: 0,
			shift_y: 0.0,
			flip_x: false,
			flip_y: false,
			scale_x: 1.0,
			scale_y: 1.0,
		}
	}
}

/**
 * Execute one or many transforms on a signal. Transforms can be defined in a `SignalTransform`.
 * Default values will not affect the input signal at all.
 */
pub fn transform(mut sig: Vec<f32>, params: &SignalTransform) -> Vec<f32> {
	sig = shift_x(sig, params.shift_x);
	sig = shift_y(sig, params.shift_y);

	let mut scale_x_factor = params.scale_x;
	if params.flip_x {
		scale_x_factor *= -1.0;
	}

	let mut scale_y_factor = params.scale_y;
	if params.flip_y {
		scale_y_factor *= -1.0;
	}

	sig = scale_x(sig, scale_x_factor);
	sig = scale_y(sig, scale_y_factor);

	return sig;
}

/**
 * Offset sample indexes by a fixed amount.
 * If `delta_x` is positive, zeroes will be inserted at the beginning of the array.
 * If `delta_x` is negative, resultant negative indexes will be wrapped to the end of the array.
 */
pub fn shift_x(mut sig: Vec<f32>, delta_x: isize) -> Vec<f32> {
	if delta_x > 0 {
		sig.extend(vec![0.0; delta_x as usize]);
		sig.rotate_right(delta_x as usize);
		sig
	} else if delta_x < 0 {
		sig.rotate_left(delta_x.abs() as usize);
		sig
	} else {
		sig
	}
}

/**
 * Multiply all sample indexes by a given factor.
 * If stretching, pad gaps with zeros.
 * If shrinking, remove indexes evenly across the signal. No transformations will be made in the 
 * y-dimension.
 * If flipping (negative `factor`), the output will be reversed. Pass -1 to reverse a signal.
 */
pub fn scale_x(mut sig: Vec<f32>, factor: f64) -> Vec<f32> {
	if factor == 0.0 { // don't waste CPU time on silly bullshit
		return vec![0.0; 0]
	} else if factor < 0.0 {
		sig.reverse();
	}
	if factor.abs() == 1.0 {
		return sig
	}

	let sig_out_len = ((sig.len() as f64 * factor.abs()) + 0.5) as usize;
	let mut sig_out = vec![0.0; sig_out_len];
	if sig_out_len < sig.len() { // loop through smaller array for efficiency and accuracy
		for i_out in 0..sig_out_len {
			let ratio_i_len: f64 = (i_out as f64) / (sig_out_len as f64);
			let i_base: usize = ((sig.len() as f64 * ratio_i_len) + 0.5) as usize;
			sig_out[i_out] = sig[i_base]
		};
	} else {
		for i_base in 0..sig.len() {
			let i_out = (((i_base as f64) * factor.abs()) + 0.5) as usize;
			sig_out[i_out] = sig[i_base];
		}
	}

	return sig_out
}

/**
 * Offset sample amplitude by a fixed amount. Signal will not be clipped.
 */
pub fn shift_y(mut sig: Vec<f32>, delta_y: f32) -> Vec<f32> {
	sig.iter_mut().for_each(|val| *val += delta_y);
	sig
}

/**
 * Multiply all samples by a given factor. Signal will not be clipped.
 */
pub fn scale_y(mut sig: Vec<f32>, factor: f32) -> Vec<f32> {
	sig.iter_mut().for_each(|val| *val *= factor);
	sig
}

/**
 * Convolve two signals in the time domain. Mathematically, this is a cross-product of two vectors. 
 */
pub fn convolve(sig_base: &Vec<f32>, sig_impulse: &Vec<f32>) -> Vec<f32> {
	let sig_out_len = sig_base.len() + sig_impulse.len() - 1;
	let mut sig_out: Vec<f32> = vec![0.0; sig_out_len];

	for i_out in 0..sig_out_len {
		for i_impulse in 0..sig_impulse.len() {
			if i_impulse > i_out { continue; }
			
			let i_base = i_out - i_impulse;
			if i_base >= sig_base.len() { continue; }

			sig_out[i_out] += sig_base[i_base] * sig_impulse[i_impulse];
		}
	}

	return sig_out;
}



#[cfg(test)]
mod tests {
	use super::*;
	use approx::assert_relative_eq;
use std::vec;

	#[test]
	fn shift_x_positive() {
		let sig_base = gen_signal();
		let sig_expected = vec![0.0, 0.0, 0.1, 0.3, 0.6, 0.8, 0.9, 0.6, 0.0, -0.3, -1.0, -0.95];

		let sig_in = sig_base.clone();
		let sig_result = shift_x(sig_in, 2);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn shift_x_negative() {
		let sig_base = gen_signal();
		let sig_expected = vec![0.8, 0.9, 0.6, 0.0, -0.3, -1.0, -0.95, 0.1, 0.3, 0.6];

		let sig_in = sig_base.clone();
		let sig_result = shift_x(sig_in, -3);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn shift_x_zero() {
		let sig_base = gen_signal();

		let sig_in = sig_base.clone();
		let sig_result = shift_x(sig_in, 0);
		assert_eq!(sig_base, sig_result);
	}

	#[test]
	fn shift_y_expected() {
		let sig_base = gen_signal();
		let sig_expected = vec![0.6, 0.8, 1.1, 1.3, 1.4, 1.1, 0.5, 0.2, -0.5, -0.45];

		let sig_in = sig_base.clone();
		let sig_result = shift_y(sig_in, 0.5);
		for i in 0..sig_result.len() {
			assert_relative_eq!(sig_expected[i], sig_result[i]);
		}
	}

	#[test]
	fn shift_y_zero() {
		let sig_base = gen_signal();

		let sig_in = sig_base.clone();
		let sig_result = shift_y(sig_in, 0.0);
		assert_eq!(sig_base, sig_result);
	}

	#[test]
	fn scale_x_stretch() {
		let sig_base = gen_signal();
		let sig_expected = vec![0.1, 0.0, 0.3, 0.0, 0.6, 0.0, 0.8, 0.0, 0.9, 0.0, 0.6, 0.0, 0.0, 0.0, -0.3, 0.0, -1.0, 0.0, -0.95, 0.0];
	
		let sig_in = sig_base.clone();
		let sig_result = scale_x(sig_in, 2.0);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn scale_x_stretch_negative() {
		let sig_base = gen_signal();
		let sig_expected = vec![-0.95, 0.0, -1.0, 0.0, -0.3, 0.0, 0.0, 0.0, 0.6, 0.0, 0.9, 0.0, 0.8, 0.0, 0.6, 0.0, 0.3, 0.0, 0.1, 0.0];
	
		let sig_in = sig_base.clone();
		let sig_result = scale_x(sig_in, -2.0);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn scale_x_shrink() {
		let sig_base = gen_signal();
		let sig_expected = vec![0.1, 0.6, 0.9, 0.0, -1.0];
	
		let sig_in = sig_base.clone();
		let sig_result = scale_x(sig_in, 0.5);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn scale_x_shrink_negative() {
		let sig_base = gen_signal();
		let sig_expected = vec![-0.95, -0.3, 0.6, 0.8, 0.3];
	
		let sig_in = sig_base.clone();
		let sig_result = scale_x(sig_in, -0.5);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn scale_x_one() {
		let sig_base = gen_signal();
		let sig_in = sig_base.clone();
		assert_eq!(sig_base, scale_x(sig_in, 1.0));
	}

	#[test]
	fn scale_x_zero() {
		assert_eq!(vec![0.0; 0], scale_x(vec![0.4, 0.0, 2.0, -0.5], 0.0));
	}

	#[test]
	fn scale_x_flip() {
		let sig_base = gen_signal();
		let sig_expected = vec![-0.95, -1.0, -0.3, 0.0, 0.6, 0.9, 0.8, 0.6, 0.3, 0.1];

		let sig_in = sig_base.clone();
		let sig_result = scale_x(sig_in, -1.0);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn scale_x_three() {
		let sig_base = gen_signal();
		let sig_expected = vec![0.1, 0.0, 0.0, 0.3, 0.0, 0.0, 0.6, 0.0, 0.0, 0.8, 0.0, 0.0, 0.9, 0.0, 0.0, 0.6, 0.0, 0.0, 0.0, 0.0, 0.0, -0.3, 0.0, 0.0, -1.0, 0.0, 0.0, -0.95, 0.0, 0.0];

		let sig_in = sig_base.clone();
		let sig_result = scale_x(sig_in, 3.0);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn scale_x_third() {
		let sig_base = gen_signal();
		let sig_expected = vec![0.1, 0.8, -0.3]; // notice we round up for i == 2

		let sig_in = sig_base.clone();
		let sig_result = scale_x(sig_in, 1.0 / 3.0);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn scale_x_inverse_len() {
		let sig_base = gen_signal();
		let scale_factor: f64 = 1.0 / (sig_base.len() as f64);
		let sig_expected = vec![0.1];

		let sig_in = sig_base.clone();
		let sig_result = scale_x(sig_in, scale_factor);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn scale_y_positive() {
		let sig_base = gen_signal();
		let sig_expected = vec![0.05, 0.15, 0.3, 0.4, 0.45, 0.3, 0.0, -0.15, -0.5, -0.475];

		let sig_in = sig_base.clone();
		let sig_result = scale_y(sig_in, 0.5);
		for i in 0..sig_result.len() {
			assert_relative_eq!(sig_expected[i], sig_result[i]);
		}
	}

	#[test]
	fn scale_y_negative() {
		let sig_base = gen_signal();
		let sig_expected = vec![-0.05, -0.15, -0.3, -0.4, -0.45, -0.3, 0.0, 0.15, 0.5, 0.475];

		let sig_in = sig_base.clone();
		let sig_result = scale_y(sig_in, -0.5);
		for i in 0..sig_result.len() {
			assert_relative_eq!(sig_expected[i], sig_result[i]);
		}
	}

	#[test]
	fn scale_y_zero() {
		let sig_base = gen_signal();
		let sig_expected = vec![0.0; 10];

		let sig_in = sig_base.clone();
		let sig_result = scale_y(sig_in, 0.0);
		assert_eq!(sig_expected, sig_result);
	}
	
	#[test]
	fn scale_y_one() {
		let sig_base = gen_signal();
		let sig_in = sig_base.clone();
		assert_eq!(sig_base, scale_y(sig_in, 1.0));
	}

	#[test]
	fn multi_transform_default() {
		let sig_base = gen_signal();
		let sig_in = sig_base.clone();
		let transform_params = SignalTransform { ..Default::default() };

		let sig_result = transform(sig_in, &transform_params);
		assert_eq!(sig_base, sig_result);
	}

	#[test]
	fn multi_transform_expected() {
		let sig_base = gen_signal();
		let sig_in = sig_base.clone();
		let transform_params = SignalTransform { 
			shift_x: 2,
			scale_x: 0.5,
			flip_x: true,
			flip_y: true,
			..Default::default() 
		};

		let sig_expected = vec![0.95, 0.3, -0.6, -0.8, -0.3, 0.0];
		let sig_result = transform(sig_in, &transform_params);
		assert_eq!(sig_expected, sig_result);
	}

	#[test]
	fn convolve_len() {
		let sig_base = gen_signal();
		let sig_impulse: Vec<f32> = vec![1.0, 1.0, 1.0, 1.0, 1.0];
		let expected_len = sig_base.len() + sig_impulse.len() - 1;

		let result = convolve(&sig_base, &sig_impulse);
		assert_eq!(expected_len, result.len());
	}

	#[test]
	fn convolve_pulse() {
		let sig_base = gen_signal();
		let sig_impulse = vec![1.0];
		let expected_output = sig_base.clone();

		let result = convolve(&sig_base, &sig_impulse);
		assert_eq!(expected_output, result);
	}

	#[test]
	fn convolve_all_1s() {
		let sig_base = gen_signal();
		let sig_impulse: Vec<f32> = vec![1.0; 5];
		let expected_output = vec![0.1, 0.4, 1.0, 1.8, 2.7, 3.2, 2.9, 2.0, 0.2, -1.65, -2.25, -2.25, -1.95, -0.95];

		let result = convolve(&sig_base, &sig_impulse);

		for i in 0..sig_base.len() {
			assert_relative_eq!(expected_output[i], result[i]);
		}
	}

	#[test]
	fn convolve_all_0s() {
		let sig_base = gen_signal();
		let sig_impulse: Vec<f32> = vec![0.0; 5];
		let expected_len = sig_base.len() + sig_impulse.len() - 1;
		let expected_output: Vec<f32> = vec![0.0; expected_len];

		let result = convolve(&sig_base, &sig_impulse);

		assert_eq!(expected_output, result);
	}

	fn gen_signal() -> Vec<f32> {
		vec![0.1, 0.3, 0.6, 0.8, 0.9, 0.6, 0.0, -0.3, -1.0, -0.95]
	}
}
