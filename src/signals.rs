use std::f64::consts::PI;

pub enum SimpleSig {
	Sine,
	Square,
	Saw,
	Triangle,
	WhiteNoise,
}

pub fn gen_sig(sig_type: SimpleSig, num_samps: usize, samps_per_cycle: usize) -> Vec<f32> {
	if !matches!(sig_type, SimpleSig::Sine) { unimplemented!(); }

	let mut sig_out: Vec<f32> = Vec::new();
	let	rads_per_samp: f64 = PI * 2.0 / (samps_per_cycle as f64);

	for i in 0..num_samps {
		let i_wrap = i % samps_per_cycle;
		let rads_curr = (i_wrap as f64) * rads_per_samp;
		sig_out.push(rads_curr.sin() as f32);
	}

	return sig_out;
}


#[cfg(test)]
mod tests {
	use super::*;
	use approx::assert_relative_eq;

	#[test]
	fn gen_sig_expected_len() {
		let len_expected: usize = 10;
		let sig_result = gen_sig(SimpleSig::Sine, len_expected, len_expected);

		assert_eq!(len_expected, sig_result.len());
	}

	#[test]
	fn gen_sig_expected_sine_quad_1() {
		let sig_expected: Vec<f32> = vec![0.0, 0.087156, 0.173648, 0.258819, 0.34202, 0.422618, 0.5, 0.573576, 0.642788, 0.707107, 0.766044, 0.819152, 0.866025, 0.906308, 0.939693, 0.965926, 0.984808, 0.996195, 1.0];
		let sig_result = gen_sig(SimpleSig::Sine, 90 / 5, 360 / 5);

		for i in 0..sig_result.len() {
			let round_offset = 10_i32.pow(6) as f32;
			let samp_approx = (sig_result[i] * round_offset).round() / round_offset;
			assert_relative_eq!(sig_expected[i], samp_approx);
		}
	}

	#[test]
	fn gen_sig_expected_sine_full() {
		let sig_len = 360;
		let sig_result = gen_sig(SimpleSig::Sine, sig_len + 1, sig_len);

		let quad_len = sig_len / 4;
		for i in 0..quad_len {
			let samp_quad_1 = sig_result[i];
			let samp_quad_2 = sig_result[quad_len * 2 - i];
			let samp_quad_3 = sig_result[quad_len * 2 + i];
			let samp_quad_4 = sig_result[quad_len * 4 - i];

			assert_relative_eq!(samp_quad_1, samp_quad_2);
			assert_relative_eq!(samp_quad_1, -samp_quad_3);
			assert_relative_eq!(samp_quad_3, samp_quad_4);
		}
	}

	#[test]
	fn gen_sig_expected_sine_repeat() {
		let cycle_len = 360;
		let num_cycles = 3;
		let sig_result = gen_sig(SimpleSig::Sine, cycle_len * num_cycles, cycle_len);

		for i in 0..cycle_len {
			let samp_cycle_1 = sig_result[i];
			let samp_cycle_2 = sig_result[cycle_len + i];
			let samp_cycle_3 = sig_result[cycle_len * 2 + i];

			assert_eq!(samp_cycle_1, samp_cycle_2);
			assert_eq!(samp_cycle_1, samp_cycle_3);
		}
	}
}
