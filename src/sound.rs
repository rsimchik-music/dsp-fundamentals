/**
 * Flexible container for transporting multiple forms of musical data at the same time. Intended to
 * represent the same music in different forms, though this is not a hard restriction.
 */
pub struct SoundEvent {
	pub start_time: f64,
	pub duration: f64,
	pub signal: Option<Signal>,
	pub file_slice: Option<FileSlice>,
	pub notes: Option< Vec<Note> >,
	pub midi: Option< Vec<MidiNote> >,
	pub midi_player: Option<MidiPlayer>,
	pub children: Option< Vec<SoundEvent> >,
}

impl Default for SoundEvent {
	fn default() -> Self {
		Self { 
			start_time: 0.0,
			duration: 0.0,
			signal: None,
			file_slice: None,
			notes: None,
			midi: None,
			midi_player: None,
			children: None,
		}
	}
}

/**
 * A raw sound wave. Samples are represented by `f64`, usually in the range `[-1, 1]`.
 */
pub struct Signal {
	pub samples: Vec<f64>,
	pub context: SignalContext,
}

/**
 * Data which is above a Signal's scope, but changes the the object's interpretation.
 */
pub struct SignalContext {
	pub sample_rate: f64,
	pub bit_depth: usize,
}

/**
 * A single note on a music staff.
 */
pub struct Note {
	pub pitch: u8,
	pub octave: u8,
	pub duration: f64,
	pub context: NoteContext,
}

/**
 * Data which is above a Note's scope, but changes the the object's interpretation.
 */
pub struct NoteContext {
	pub notes_per_octave: u8,
	pub least_significant_subdivision: u16,
	pub tempo: Option<u16>,
	pub meter: Option< (u8,u8) >,
	pub key: Option<i8>,
}

/**
 * A single note in a midi file.
 */
pub struct MidiNote {
	pub pitch: u8,
	pub context: MidiContext
}

/**
 * Data which is above a MidiNote's scope, but changes the the object's interpretation.
 */
pub struct MidiContext {
	pub notes_per_octave: u8,
}

/**
 * A sound source capable of interpreting midi data.
 */
pub struct MidiPlayer {
	// pub vst_plugin: Option<VSTPlugin>
	// pub synth: Option<Synthesizer>
}

/**
 * A window into an audio file.
 */
pub struct FileSlice {
	pub start_time_in_file: f64,
	// pub file: SoundFile
}
