pub fn midinote_to_freq(midinote: u8) -> f32 {
	const NOTES_PER_OCTAVE: u8 = 12;
	const A4_FREQ: f32 = 440.0;
	const A4_MIDI: i16 = 69;

	let semitone_ratio = semitone_ratio(NOTES_PER_OCTAVE);
	let note_diff: i16 = i16::from(midinote) - A4_MIDI;
	return A4_FREQ * semitone_ratio.powf(f32::from(note_diff));
}

pub fn semitone_ratio(notes_per_octave: u8) -> f32 {
	return 2_f32.powf(1.0 / (notes_per_octave as f32));
}

#[cfg(test)]
mod tests {
	use approx::relative_eq;
	use super::*;

	#[test]
	fn midinote_to_freq_high() {
		const E7: u8 = 100;
		const E7_FREQ: f32 = 2637.0;

		let result = midinote_to_freq(E7);
		relative_eq!(result, E7_FREQ, epsilon = f32::EPSILON);
	}

	#[test]
	fn midinote_to_freq_low() {
		const E1: u8 = 28;
		const E1_FREQ: f32 = 41.203;

		let result = midinote_to_freq(E1);
		relative_eq!(result, E1_FREQ, epsilon = f32::EPSILON);
	}
}
